Walking Skeleton
================

This is a simple example to ilustrate the walking skeleton concept.
The development environment with Vagrant, check the Vagrantfile in the root directory.

Develop: [![pipeline status](https://gitlab.com/nicopaez/walkingskeleton/badges/develop/pipeline.svg)](https://gitlab.com/nicopaez/walkingskeleton/commits/develop)

Master: [![pipeline status](https://gitlab.com/nicopaez/walkingskeleton/badges/master/pipeline.svg)](https://gitlab.com/nicopaez/walkingskeleton/commits/master)