require 'rspec'
require_relative '../model/mail_merger'

describe 'MailMerger' do

  it 'should send with adapter' do
    to = 'john@example.com'
    body = 'some body'
    adapter = double('mail_adapter')
    expect(adapter).to receive(:send).with(to, body) { 1 }
    
    mail_merger = MailMerger.new(adapter)
    result = mail_merger.send(to, body)
    expect(result).to eq 1
  end

end
