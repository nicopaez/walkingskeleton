require 'byebug'

class MailMerger

  def initialize(mail_adapter)
    @mail_adapter =  mail_adapter
  end

  def send(to, template)
    mail_body = template
    @mail_adapter.send(to, mail_body)
    return 1
  end
end