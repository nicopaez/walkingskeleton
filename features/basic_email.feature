Feature: Basic email without placeholders

  Scenario: Send mail successfully
    Given I have template "hello everyone!"
    When I send it to "john@example.com"
    Then one mail is delivered