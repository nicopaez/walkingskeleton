Given("I have template {string}") do |template|
  @mail_template = template
end

When("I send it to {string}") do |to|
  body = { :to => to, :template => @mail_template }.to_json
  post '/mail', body, "CONTENT_TYPE" => "application/json"
end

Then("one mail is delivered") do
  expect(last_response.status).to be == 200
  expect(JSON.parse(last_response.body)['mails_sent_count']).to eq 1
end