When("send a request to \/") do
  get '/'
end

Then("the response has content-type application-json") do
  expect(last_response.status).to be == 200
  expect(last_response.content_type).to eq 'application/json'
end