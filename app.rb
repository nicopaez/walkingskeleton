require 'sinatra'
require 'json'
require_relative './model/mail_merger'
require_relative './adapters/smtp_adapter'

VERSION = '0.0.1'

set :bind, '0.0.0.0'

before do
  content_type 'application/json'
end

get '/' do
  { :content => 'app running' }.to_json
end

get '/version' do
  { :version => VERSION }.to_json
end

post '/mail' do
  input = JSON.parse(request.body.read)
  to = input['to']
  template = input['template']
  mails_sent_count = mail_merger.send(to, template)
  { :mails_sent_count => mails_sent_count }.to_json
end

def mail_merger
  mail_adapter = SmtpAdapter.new(ENV['RACK_ENV']=='test')
  MailMerger.new(mail_adapter)
end