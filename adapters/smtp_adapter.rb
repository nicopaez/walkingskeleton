require 'pony'

class SmtpAdapter

  def initialize(test_mode)
    @mode = test_mode ? :test : :smtp
    @options = {
       :address        => 'smtp.sendgrid.net',
        :port           => '25',
        :user_name      => 'apikey',
        :password       =>  ENV['SMTP_PASSWORD'],
        :authentication => :plain
    }
  end

  def send(to, body)
    Pony.mail({
      :to => to,
      :subject => 'From mail merger',
      :body => body,
      :via => @mode,
      :via_options => @options
      })
  end

end

